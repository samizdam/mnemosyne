<?php
require_once '../vendor/autoload.php';

define('STORAGE_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'storage');
define('MODEL_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'models');

use Composer\Autoload\ClassLoader;
$loader = new ClassLoader();
$loader->set('', MODEL_PATH);
$loader->register();