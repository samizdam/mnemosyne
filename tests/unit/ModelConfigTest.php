<?php

use samizdam\Mnemosyne\ModelConfig;
class ModelConfigTest extends PHPUnit_Framework_TestCase{
	
	public function testConfigCreate(){
		$modelConfig = new ModelConfig('SimpleModel', ['primaryKey' => 'id']);
		
		$this->assertTrue($modelConfig->hasPrimaryKey());
		
		$emptyModelConfig = new ModelConfig('Dummy');
		$this->assertFalse($emptyModelConfig->hasPrimaryKey());
	}
	
	
}