<?php

use samizdam\Mnemosyne\drivers\File\Directory;
class FileDirectoryTest extends PHPUnit_Framework_TestCase{
	
	protected $pathname;
	
	public function __construct(){
		$this->pathname = STORAGE_PATH . DIRECTORY_SEPARATOR . 'SimpleModel';
	}
	
	public function testOpenDir(){
		$this->assertFileNotExists($this->pathname);
		$directory = new Directory($this->pathname);
		$this->assertFileExists($this->pathname);
	}
	
	
	
	public function testDirectoryCleanUp(){
		$foo = $this->pathname . DIRECTORY_SEPARATOR . 'foo';
		$bar = $this->pathname . DIRECTORY_SEPARATOR . 'bar'; 

		$this->assertFileNotExists($foo);
		$this->assertFileNotExists($bar);
		
		$directory = new Directory($this->pathname);
		touch($foo);
		touch($bar);
		
		$this->assertFileExists($foo);
		$this->assertFileExists($bar);
		
		$directory->cleanUp();
		
		$this->assertFileNotExists($foo);
		$this->assertFileNotExists($bar);
		
	}
	
	
	protected function setUp(){
		$this->cleanUp();
	}

	protected function tearDown(){
		$this->cleanUp();
	}
	
	/**
	 * cleanup storage
	 */
	protected function cleanUp(){
		if(file_exists($this->pathname)){
			foreach (glob($this->pathname . DIRECTORY_SEPARATOR . '*') as $filename){
				unlink($filename);
			}
			
			rmdir($this->pathname);
		} 
	}
}