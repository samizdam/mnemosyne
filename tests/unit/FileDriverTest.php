<?php

use samizdam\Mnemosyne\drivers\File\Driver;
use samizdam\Mnemosyne\Container\Container;
use samizdam\Mnemosyne\MetaInfo;
use ModelNS\AdvancedModel;
use samizdam\Mnemosyne\drivers\File\Directory;

class FileDriverTest extends PHPUnit_Framework_TestCase{
	
	protected $simpleObject;
	
	protected $simpleObjectPath;
	
	protected $advansedObject;
	
	protected $advansedObjectPath;
	
	protected function setUp(){
		$this->simpleObject = new SimpleModel();
		
		$this->simpleObjectPath = STORAGE_PATH . DIRECTORY_SEPARATOR . 'SimpleModel';
		
		$this->advansedObject = new AdvancedModel();
		$this->advansedObjectPath = STORAGE_PATH . DIRECTORY_SEPARATOR
		. str_replace('\\', DIRECTORY_SEPARATOR, get_class($this->advansedObject));
		
		
		$simpleDir = new Directory($this->simpleObjectPath);
		$simpleDir->cleanUp();
		
		$advancedDir = new Directory($this->advansedObjectPath);
		$advancedDir->cleanUp();
	}
	
	public function testWriteWithoutNS(){
		$driver = new Driver(STORAGE_PATH);
		$container = new Container($this->simpleObject);
		
		$filename = $this->simpleObjectPath . DIRECTORY_SEPARATOR
			. $container->getMetaInfo()->getHash();
		
		$this->assertFileNotExists($filename);
		$driver->write($container);
		$this->assertFileExists($filename);
	}
	
	public function testReadWithoutNS(){
		$driver = new Driver(STORAGE_PATH);
		
		$container = new Container($this->simpleObject);
		$driver->write($container);
		
		$metaInfo = new MetaInfo($this->simpleObject);

		$object = $driver->read($metaInfo);
		
		$this->assertEquals($this->simpleObject, $object);
		$this->assertNotEquals($this->advansedObject, $object);
	}	
	
	public function testDeleteWithoutNS(){
		$driver = new Driver(STORAGE_PATH);

		$container = new Container($this->simpleObject);
		$driver->write($container);
		
		$filename = $this->simpleObjectPath . DIRECTORY_SEPARATOR
		. $container->getMetaInfo()->getHash();
		
		$this->assertFileExists($filename);
		
		$metaInfo = new MetaInfo($this->simpleObject);
		$driver->delete($metaInfo);
		
		$this->assertFileNotExists($filename);
	}
	
	public function testWriteWithNS(){
		$driver = new Driver(STORAGE_PATH);

		$container = new Container($this->advansedObject);
		
		$filename = $this->advansedObjectPath . DIRECTORY_SEPARATOR
		. $container->getMetaInfo()->getHash();
		
		$this->assertFileNotExists($filename);
		$driver->write($container);
		$this->assertFileExists($filename);
		
	}
	
	public function testReadWithNS(){
		$driver = new Driver(STORAGE_PATH);
		
		$container = new Container($this->advansedObject);
		$driver->write($container);
		
		$metaInfo = new MetaInfo($this->advansedObject);
		
		$object = $driver->read($metaInfo);
		
		$this->assertEquals($this->advansedObject, $object);
		$this->assertNotEquals($this->simpleObject, $object);		
	}
	
	public function testDeleteWithNS(){
		$driver = new Driver(STORAGE_PATH);
		
		$container = new Container($this->advansedObject);
		$driver->write($container);
		
		$filename = $this->advansedObjectPath . DIRECTORY_SEPARATOR
		. $container->getMetaInfo()->getHash();
		
		$this->assertFileExists($filename);
		
		$metaInfo = new MetaInfo($this->advansedObject);
		$driver->delete($metaInfo);
		
		$this->assertFileNotExists($filename);		
	}
	
	
}