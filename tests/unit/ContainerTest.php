<?php

use samizdam\Mnemosyne\Container\Container;
class ContainerTest extends PHPUnit_Framework_TestCase{
		
	public function testContainerCreate(){
		$model = new SimpleModel();
		$container = new Container($model);
		$this->assertEquals($container->extract(), $model);
	}
	
	public function testObjectVars(){
		$model = new SimpleModel();
		$model->id = 123;
		$container = new Container($model);
		$this->assertArrayHasKey('id', $container->getObjectVars());
		$this->assertEquals($container->getObjectVars()['id'], $model->id);
		$this->assertNotEquals($container->getObjectVars()['id'], 666);
		$this->assertArrayNotHasKey('bad-array-key', $container->getObjectVars());
	} 
	
}