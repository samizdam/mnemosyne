<?php


use samizdam\Mnemosyne\MetaInfo;
use samizdam\Mnemosyne\ModelConfig;
class MetaInfoTest extends PHPUnit_Framework_TestCase{
	
	public function testMetaInfoCreate(){
		$config = new ModelConfig('SimpleModel', ['primaryKey' => 'id']);

		$object = new SimpleModel();
		$id = 100500;
		$object->id = $id;
		
		$metaInfo = new MetaInfo($object, $config);
		$this->assertEquals($metaInfo->getClass(), 'SimpleModel');
		$this->assertEquals($metaInfo->getPrimaryKeyValue(), $id);
	}
	
	public function testMetaInfoHash(){
		$objectA = new SimpleModel();
		$objectB = new SimpleModel();
		
		$this->assertEquals($objectA, $objectB);
		
		$metaInfoA = new MetaInfo($objectA);
		$metaInfoB = new MetaInfo($objectB);
		
		$this->assertNotEquals($metaInfoA, $metaInfoB);
		
		$metaInfoA2 = new MetaInfo($objectA);
		$this->assertEquals($metaInfoA->getHash(), $metaInfoA2->getHash());
		
	}
}