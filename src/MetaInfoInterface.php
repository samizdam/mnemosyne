<?php
namespace samizdam\Mnemosyne;
/**
 * 
 * @author samizdam
 *
 */
interface MetaInfoInterface{
	public function getClass();
	
	public function getPrimaryKeyValue();
	
	public function getHash();
	
	public function getCreateTime();
	
	public function getModifiedTime();
	
	public function isNewObject();

}