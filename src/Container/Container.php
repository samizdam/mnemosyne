<?php
namespace samizdam\Mnemosyne\Container;

use samizdam\Mnemosyne\Exception\DomainException;
use samizdam\Mnemosyne\ModelConfig;
use samizdam\Mnemosyne\MetaInfo;
class Container implements ContainerInterface{
	protected $object;
	protected $metaInfo;
	
	public function __construct(&$object, ModelConfig $config = null){
		if(!is_object($object)){
			throw new DomainException(__METHOD__.' expects 1 to be object, '.gettype($object).' given.');
		}
		$this->object = &$object;
		$this->setMetaInfo($config);
		
	}
	
	public function extract(){
		return $this->object;
	}
	
	public function getObjectVars(){
		return get_object_vars($this->object);
	}
	
	protected function setMetaInfo(ModelConfig $config = null){
		$this->metaInfo = new MetaInfo($this->object, $config);
	}
	
	public function getMetaInfo(){
		return $this->metaInfo;
	}
}