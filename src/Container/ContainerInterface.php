<?php
namespace samizdam\Mnemosyne\Container;

use samizdam\Mnemosyne\MetaInfoInterface;
interface ContainerInterface{
	
	public function extract();
	/**
	 * @return MetaInfoInterface
	 */
	public function getMetaInfo();
	
	public function getObjectVars();
}