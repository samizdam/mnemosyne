<?php
namespace samizdam\Mnemosyne;
/**
 * 
 * @author samizdam
 *
 */
class MetaInfo implements MetaInfoInterface{
	
	protected $class;
	protected $className;
	protected $classNamespace;
	protected $object;
	
	/**
	 * 
	 * @var ModelConfig
	 */
	protected $config;
	
	protected $create_time;
	
	protected $hash;

	public function __construct(&$object, ModelConfig $config = null){
		$this->object = &$object;
		
		$this->class = get_class($object);
		$this->className = basename($this->class);
		$this->classNamespace = (dirname($this->class) === '.')?null:dirname($this->class);
		
		$this->config = empty($config)?new ModelConfig($this->class):$config;
		
		$this->hash = $this->generateHash();
	}
	
	public function getClass(){
		return $this->class;
	}
	
	public function getPrimaryKeyValue(){
		if($this->config->hasPrimaryKey()){
			return $this->object->{$this->config->getPrimaryKey()};
		}else{
			return null;
		}
	}
	
	public function isNewObject(){
		throw new Exception\NotImplementedException(__METHOD__ . ' not implement.');
	}
	
	public function getCreateTime(){
		if(empty($this->create_time)){
			$this->create_time = time();
		}
		return $this->create_time;
	}
	
	public function getModifiedTime(){
		throw new Exception\NotImplementedException(__METHOD__ . ' not implement.');
	}
	
	protected function generateHash(){
		$pk = $this->getPrimaryKeyValue();
		if(is_null($pk)){
			$this->hash = md5($this->getClass() . spl_object_hash($this->object));
		}else{
			$this->hash = md5($this->getClass() . $pk);
		}
		
		return $this->hash;
	}
	
	public function getHash(){
		return $this->hash;
	}
}