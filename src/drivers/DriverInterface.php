<?php
namespace samizdam\Mnemosyne\drivers;

use samizdam\Mnemosyne\Container\ContainerInterface;
use samizdam\Mnemosyne\MetaInfoInterface;

interface DriverInterface{
	
	public function write(ContainerInterface $container);
	
	public function read(MetaInfoInterface $metaInfo);
	
	public function delete(MetaInfoInterface $metaInfo);
}