<?php
namespace samizdam\Mnemosyne\drivers\File;

use samizdam\Mnemosyne\drivers\DriverInterface;
use samizdam\Mnemosyne\Container\ContainerInterface;
use samizdam\Mnemosyne\MetaInfoInterface;

class Driver implements DriverInterface{
	protected $storagePath;
	
	public function __construct($storagePath){
		$this->storagePath = $storagePath;
	}
	
	public function write(ContainerInterface $container){
		$pathname = $this->storagePath
			. DIRECTORY_SEPARATOR 
			. str_replace('\\', DIRECTORY_SEPARATOR, $container->getMetaInfo()->getClass());
		$directory = new Directory($pathname);
		
		$filename = $pathname 
			. DIRECTORY_SEPARATOR
			. $container->getMetaInfo()->getHash();
		
		$data = serialize($container->extract());
		
		file_put_contents($filename, $data);
	}
	
	public function read(MetaInfoInterface $metaInfo){
		$filename = $this->getFilenameByMetaInfo($metaInfo);
		return unserialize(file_get_contents($filename));
	}
	
	public function delete(MetaInfoInterface $metaInfo){
		$filename = $this->getFilenameByMetaInfo($metaInfo);
		unlink($filename);
	}
	
	protected function getFilenameByMetaInfo(MetaInfoInterface $metaInfo){
		$filename = $this->storagePath
		. DIRECTORY_SEPARATOR
		. str_replace('\\', DIRECTORY_SEPARATOR, $metaInfo->getClass())
		. DIRECTORY_SEPARATOR
		. $metaInfo->getHash();
		return $filename;
	}
	
}