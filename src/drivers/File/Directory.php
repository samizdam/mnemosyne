<?php
namespace samizdam\Mnemosyne\drivers\File;

use samizdam\Mnemosyne\Exception\UnexpectedValueException;
use samizdam\Mnemosyne\Exception\RuntimeException;
use samizdam\Mnemosyne\drivers\DirectoryInterface;

class Directory implements DirectoryInterface{
	
	protected $directory;
	const MODE = 0777;
	 
	public function __construct($pathname){
		$this->openDirectory($pathname);
	}
	
	public function cleanUp(){
		foreach ($this->directory as $file){
			if($file->isFile()){
				unlink($file->getPathname());
			}
		}
		
		rmdir($this->directory->getPath());
	}
	
	protected function openDirectory($pathname){ 		
		if(!is_dir($pathname)){
			if(!mkdir($pathname, self::MODE, true)){
				throw new UnexpectedValueException('Failed to mkdir ' . $pathname);
			}
		}
		
		$this->directory = new \DirectoryIterator($pathname);
		
		if(!$this->directory->isWritable()){
			throw new RuntimeException($pathname .' is not writable');
		}
	}
}