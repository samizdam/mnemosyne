<?php
namespace samizdam\Mnemosyne\traits;

trait ImmutableSetOfProperties{
	public function __set($property, $value){
		if(property_exists($this, $property)){
				$this->{$property} = $value;
		}else{
			throw new \DomainException("Attempt to set value to undefined property \${$property} of ".__CLASS__." class");
		}
	}
}