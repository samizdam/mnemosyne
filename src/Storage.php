<?php
namespace samizdam\Mnemosyne;

use samizdam\Mnemosyne\Exception\UnexpectedValueException;
use samizdam\Mnemosyne\Exception\NotImplementedException;
use samizdam\Mnemosyne\drivers\Directory;
use samizdam\Mnemosyne\Query\QueryInterface;
use samizdam\Mnemosyne\Container\Container;
use samizdam\Mnemosyne\Collection\Collection;
/**
 * 
 * @author samizdam
 *
 */
class Storage implements StorageInterface{
	private $driverName;
	
	/**
	 * TODO make it special object
	 * @var unknown
	 */
	protected $config;
	/**
	 * 
	 * @var drivers\DriverInterface
	 */
	private $driver;
	
	
	/**
	 * TODO refactoring configuration
	 * @param unknown $path
	 * @throws UnexpectedValueException
	 */
	public function __construct($path){
		$this->config = new \stdClass();
		if(is_writable($path)){
			$this->getConfig()->dataPath = $path;
			$config_file = $this->getConfig()->dataPath . DIRECTORY_SEPARATOR . 'config.php';
			if(file_exists($config_file)){
				$configuration = require $config_file;
				if(is_array($configuration)){
					$this->setConfig($configuration);
				}
			}
		}else{
			throw new UnexpectedValueException($path . ' is not writeble');
		}
	}
	
	
	/**
	 * TODO refactoring
	 * @param array $config
	 */
	public function setConfig(array $config = []){
		foreach ($config as $name => $value){
			if($name === 'modelsConfig' && is_array($value)){
				foreach ($value as $class => $config){
					$this->setModelConfig($class, $config);
				}
			}else{
				$this->getConfig()->{$name} = $value;
			}
		}
	}
	
	public function getDriverName(){
		if(empty($this->driverName)){
			$this->driverName = __NAMESPACE__ . '\\drivers\\File';
		}
		return $this->driverName;
	}
	
	/**
	 * TODO get driver type from config
	 * @return \samizdam\Mnemosyne\drivers\DriverInterface
	 */
	public function getDriver(){
		if(empty($this->driver)){
			$driverName = $this->getDriverName();
			$this->driver = new $driverName($this->getConfig()->dataPath);
		}
		return $this->driver;
	}
	
	public function getConfig(){
		return $this->config;
	}
	
	public function put($object){
		$classDirectory = $this->getModelDirectory($object);
		$container = new Container($object, $this->getModelConfig(get_class($object)));
// 		$container->getMetaInfo()->getHash() .
		$this->getDriver()->write($container);
	}
	
	protected function getModelDirectory($object){
		$class = get_class($object);
		$class_path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
		$pathname = $this->getConfig()->dataPath() . DIRECTORY_SEPARATOR . $class_path; 
		return new Directory($pathname);
	}
	
	/**
	 * 
	 * @param QueryInterface $query
	 * @return Collection
	 */
	public function get(QueryInterface $query){
		$results = $this->getDriver()->find($query);
		return new Collection($results);
	}
	
	public function delete(QueryInterface $query){
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 * @param string $class
	 * @return ModelConfig
	 */
	public function getModelConfig($class){
		return $this->getConfig()->modelsConfig[$class];
	}
	
	public function setModelConfig($class, $config){
		$this->getConfig()->modelsConfig[$class] = new ModelConfig($class, $config);
	}
	
}