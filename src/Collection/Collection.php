<?php
namespace samizdam\Mnemosyne\Collection;

class Collection implements CollectionInterface{
	protected $results = [];
	
	public function __construct($results = []){
		$this->results = $results;
	}
}