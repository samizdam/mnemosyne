<?php
namespace samizdam\Mnemosyne\Criteria;

interface CriteriaInterface{
	const IS = '==';
	const EQUAL = '===';
	const NOT_IS = '!=';
	const NOT_EQUAL = '!==';
		
}