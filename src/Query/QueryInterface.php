<?php
namespace samizdam\Mnemosyne\Query;

interface QueryInterface{
	public function __construct($className, array $attributes = []);
	public function setPropertyValue($name, $value);
	public function setMetaKeyValue($key, $value);
	public function setLimit($limit = 0);
	public function setOffset($offset = 0);	
}