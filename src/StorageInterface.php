<?php
namespace samizdam\Mnemosyne;

interface StorageInterface{
	public function put($object);
	
	public function get(QueryInterface $query);
	
	public function delete(QueryInterface $query);	
}
