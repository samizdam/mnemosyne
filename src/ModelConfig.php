<?php
namespace samizdam\Mnemosyne;

class ModelConfig{
	use traits\ImmutableSetOfProperties;
	protected $primaryKey;
	
	const MODEL_CONFIG_METHOD = '__mnemosyne_config';
	
	public function __construct($className, array $config = []){
		$config = array_merge(
				$config, 
				method_exists($className, self::MODEL_CONFIG_METHOD)
					?call_user_func([$className, self::MODEL_CONFIG_METHOD])
					:[]
				);
		
		foreach ($config as $property => $value){
			$this->{$property} = $value;
		}
	}
	
	public function hasPrimaryKey(){
		return !empty($this->primaryKey);
	}
	
	public function getPrimaryKey(){
		return $this->primaryKey;
	}	

}